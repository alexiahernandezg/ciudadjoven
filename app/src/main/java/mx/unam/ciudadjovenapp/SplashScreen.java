package mx.unam.ciudadjovenapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/*
 * Actividad para crear la pantalla splash usando
 * un delay para mostrarla
 */
public class SplashScreen extends AppCompatActivity {

    /*
     * Actividad para mostrar la pantalla splash. Se llama a la
     * pantalla para mostrar usando un handler con un hilo que
     * cuenta el tiempo, muestra el splash y después incia la
     * actividad de inicio.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen.this, StartActivity.class));
                finish();
            }
        },2000);
    }
}