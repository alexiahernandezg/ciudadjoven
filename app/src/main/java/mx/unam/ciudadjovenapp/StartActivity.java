package mx.unam.ciudadjovenapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

/*
 * Actividad para mostrar el menú de inicio.
 */
public class StartActivity extends AppCompatActivity {

    //mostrar la pantalla de inicio.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}